package apiclient

//APIClient interface for all excahnge api
type APIClient interface {
	/* Initializes implementation fields. In the parameters it takes private 
	 * values ​​for accessing the api. If a particular implementation does not use 
	 * some parameter, when the function is called, it remains empty.
	 */
	Init(accountID string, apiKey string, apiSecret string) error

	//=============== Настройки аккаунта и инофрмация по открытым ордерам/позициям ===============

	/* Change user's position mode (Hedge Mode or One-way Mode ) 
	 *  if bool = "true": Hedge Mode mode; 
	 *  if bool = "false": One-way Mode
	 */
	ChangePositionMode(bool string) error 

	/* Change user's initial leverage of specific symbol market.
	 * leverage from 1 to 125
	 */
	ChangeLeverage(symbol string, leverage int64) error

	/* Change Margin Type (ISOLATED or CROSSED)
	 */
	ChangeMarginType(symbol, marginType string) error // ????

	/* Gets a list of all open transactions on this account, fills the structure
	 * in apiclient format and returns a pointer to this structure.
	 */
	GetMyOpenOrders(symbol string) (*[]MakedOrder, error)
	
	/* Get current position information on pair
	 */
	GetPositionInformation(symbol string) (*[]MakedPosition, error)

	/* Gets a list of pairs and currency balances, translates into apiclient format 
	 * and fills in the appropriate implementation fields. Returns a pointer to a table of implementation balances.
	 */
	GetBalances() (*map[string]Balance, error)

	//=============== Работа с рынком ===============

	/* Places a sell order.
	 * 
	 * If amount is zero, then the order must be of type market.
	 */
	Sell(symbol string, amount float64, price float64) (*MakedOrder, error)

	/* Places a buy order.
	 * 
	 * If amount is zero, then the order must be of type market.
	 */
	Buy(symbol string, amount float64, price float64) (*MakedOrder, error)

	/* Cancels an order.
	 */
	CancelOrder(symbol string, id string) error

	//=============== Получение информации с рынка ===============

	/* Gets a list of all open transactions on the exchange (not only ours), 
	 * fills the structure in apiclient format and returns a pointer to this structure.
	 *
	 * should sort asks by price increase
	 * bids sort by descending price
	 */
	GetOrderBook(symbol string) (*OrderBook, error)

	/* Kline/candlestick bars for a symbol. Klines are uniquely identified by their open time.
	 *
	 * Must be sorted from late to early
	 */
	GetKline(symbol string, period string, limit int64) (*[]KlineBook, error)
	
	/* Mark Price and Funding Rate
	 *
	 * first return value is Mark Price
	 * second return value is Funding Rate
	 */
	GetMarkPrice(symbol string) (float64, float64, error) 

	/* Funding Rate History
	 *
	 * first return value is array of Funding Rate
	 * second return value is an error during request
	 */
	GetFundingRateHistory(symbol string, limit int64) (*[]float64, error) 

	/* Get present open interest of a specific symbol.
	 *
	 * first return value is open interest
	 */
	GetOpenInterest(symbol string) (float64, error)

	/* Get statistics open interest of a specific symbol.
	 *
	 * first return value is array of open interest
	 */
	 GetOpenInterestHistory(symbol string, period string, limit int64) (*[]float64, error)

}

//Status type for enum about order status
type Status string

//Side type for enum about order buy or sell types or something like that
type Side string

//constants about Status and Side
const (
	Buy             Side   = "LONG"
	Sell            Side   = "SHORT"
	Both  			Side   = "BOTH"
	Filled          Status = "FILLED"
	NotFilled       Status = "NotFilled"
	PartiallyFilled Status = "PartiallyFilled"
)

//Balance help struct for APIClient
type Balance struct {
	Free   float64 `json:"free"`   //available balance for use in new orders
	Locked float64 `json:"locked"` //locked balance in orders
}

//OrderBook help struct for APIClient
type OrderBook struct {
	Asks []Order `json:"asks"` //asks.Price > any bids.Price
	Bids []Order `json:"bids"`
}

//Order help struct for APIClient
type Order struct {
	Quantity float64 `json:"quantity"`
	Price    float64 `json:"price"`
}

//MakedOrder help struct for APIClient
type MakedOrder struct {
	ID string `json:"id"`
	Symbol      string  `json:"symbol"`
	//  Status Should be one of apiclient.Status constants(Filled, NotFilled, PartiallyFilled)
	Status      Status  `json:"status"`
	//  Side Should be one of apiclient.Side constants(Buy, Sell)
	Side           Side    `json:"side"`
	PositionSide   string  `json:"positionSide"`
	Price          float64 `json:"price"`
	Amount         float64 `json:"amount"`
	Commission     float64 `json:"commission"`
}

//MakedPosition help struct for APIClient
type MakedPosition struct {
	Symbol        		 string  `json:"symbol"`
	Side   		   		 Side    `json:"positionSide"`
	MarginType	  		 string  `json:"marginType"`
	Price          		 float64 `json:"price"`
	LiquidationPrice	 float64 `json:"liquidationPrice"`
	Amount        		 float64 `json:"amount"`
	IsolatedMargin		 float64 `json:"isolatedMargin"`
	Profit 				 float64 `json:"profit"`
	Leverage     		 int64   `json:"leverage"`
}

//KlineBook help struct for APIClient
type KlineBook struct {
	Open        float64 `json:"open"`
	High   		float64 `json:"high"`
	Low	  		float64 `json:"low"`
	Close       float64 `json:"close"`
	Volume	  	float64 `json:"volume"`
	VolumeAsset float64 `json:"volumeAsset"`
}